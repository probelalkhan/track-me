package net.simplifiedcoding.trackme.models;

/**
 * Created by Belal on 3/5/2018.
 */

public class User {

    public String uid, name, avatarUrl;
    public double latitude, longitude;
    public String updatedAt;

    public User(){

    }

    public User(String uid, String name, String avatarUrl, double latitude, double longitude, String updatedAt) {
        this.uid = uid;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.latitude = latitude;
        this.longitude = longitude;
        this.updatedAt = updatedAt;
    }
}
