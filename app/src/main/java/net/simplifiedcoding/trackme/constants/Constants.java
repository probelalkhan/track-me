package net.simplifiedcoding.trackme.constants;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Belal on 3/4/2018.
 */

public class Constants {

    public static final String DB_REF_USERS = "Users";
    public static final String KEY_USER_ID = "key_user_id";
    public static final String KEY_STATUS = "status";

    public static String getCurrentTime() {
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    }

}