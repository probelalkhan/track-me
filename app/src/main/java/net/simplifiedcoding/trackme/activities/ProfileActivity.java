package net.simplifiedcoding.trackme.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import net.simplifiedcoding.trackme.R;
import net.simplifiedcoding.trackme.adapters.UsersAdapter;
import net.simplifiedcoding.trackme.constants.Constants;
import net.simplifiedcoding.trackme.models.User;
import net.simplifiedcoding.trackme.tracker.TrackerService;

import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends AppCompatActivity {

    GoogleSignInClient mGoogleSignInClient;

    private static final int PERMISSIONS_REQUEST = 1;
    private static String[] PERMISSIONS_REQUIRED = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    FirebaseAuth mAuth;
    DatabaseReference dbUsers;

    ProgressBar progressBar;
    ToggleButton toggleButton;

    RecyclerView recyclerViewUsers;
    UsersAdapter usersAdapter;
    List<User> userList;

    EditText editTextSearch;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Track Me");
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();
        dbUsers = FirebaseDatabase.getInstance().getReference(Constants.DB_REF_USERS);
        progressBar = findViewById(R.id.progressbar);
        editTextSearch = findViewById(R.id.editTextSearch);
        toggleButton = findViewById(R.id.buttonTrack);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //start tracking
                    Toast.makeText(ProfileActivity.this,
                            "You are being tracked...", Toast.LENGTH_LONG).show();
                    checkLocationPermission();
                } else {
                    confirmStop();
                }
            }
        });

        recyclerViewUsers = findViewById(R.id.recyclerViewUsers);
        recyclerViewUsers.setHasFixedSize(true);
        recyclerViewUsers.setLayoutManager(new LinearLayoutManager(this));
        userList = new ArrayList<>();
        usersAdapter = new UsersAdapter(this, userList);
        recyclerViewUsers.setAdapter(usersAdapter);

        User user = new User(
                mAuth.getCurrentUser().getUid(),
                mAuth.getCurrentUser().getDisplayName(),
                mAuth.getCurrentUser().getPhotoUrl().toString(),
                0,
                0,
                Constants.getCurrentTime()
        );


        dbUsers.child(user.uid).setValue(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(ProfileActivity.this,
                                    task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }

                        loadUsersFromFirebase();
                    }
                });

        if (isServiceRunning(TrackerService.class)) {
            toggleButton.setChecked(true);
        } else {
            toggleButton.setChecked(false);
        }
    }

    private void loadUsersFromFirebase() {

        dbUsers.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.GONE);
                userList.clear();
                if (dataSnapshot.exists()) {

                    editTextSearch.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            List<User> userListTemp = new ArrayList<>();

                            if (!s.toString().equals("")) {
                                for (User u : userList) {
                                    String name = u.name.toLowerCase();
                                    String search = s.toString().toLowerCase();
                                    if (name.contains(search)) {
                                        userListTemp.add(u);
                                    }
                                }

                                usersAdapter = new UsersAdapter(getApplicationContext(), userListTemp);
                                recyclerViewUsers.setAdapter(usersAdapter);
                            }
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });


                    for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                        userList.add(userSnapshot.getValue(User.class));
                    }
                    usersAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkLocationPermission() {
        int locationPermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        int storagePermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (locationPermission != PackageManager.PERMISSION_GRANTED
                || storagePermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_REQUIRED, PERMISSIONS_REQUEST);
        } else {
            checkGpsEnabled();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkGpsEnabled() {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "Make sure gps is enabled", Toast.LENGTH_LONG).show();
        } else {
            startLocationService();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void startLocationService() {
        // Before we start the service, confirm that we have extra power usage privileges.
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        Intent intent = new Intent();
        if (!pm.isIgnoringBatteryOptimizations(getPackageName())) {
            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        }

        startService(
                new Intent(this, TrackerService.class)
        );
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager)
                getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (mAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    /**
     * Callback for location permission request - if successful, run the GPS check.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            // We request storage perms as well as location perms, but don't care
            // about the storage perms - it's just for debugging.
            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(ProfileActivity.this, "You have to issue the permissions", Toast.LENGTH_LONG).show();
                    } else {
                        checkGpsEnabled();
                    }
                }
            }
        }
    }

    private void confirmStop() {
        if (isServiceRunning(TrackerService.class)) {
            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to stop tracking?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            toggleButton.setChecked(false);
                            stopLocationService();
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();

        }
    }

    private void stopLocationService() {
        stopService(new Intent(this, TrackerService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuAbout:
                displayAppInfo();
                return true;
            case R.id.menuLogout:
                mAuth.signOut();
                mGoogleSignInClient.signOut();
                finish();
                startActivity(new Intent(this, MainActivity.class));
                return true;
            default:
                return false;
        }
    }

    private void displayAppInfo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("This app is for tracking your friends live location" +
                "\nDeveloped by Students of VVIT, Purnia");
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }
}





