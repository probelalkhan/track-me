package net.simplifiedcoding.trackme.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import net.simplifiedcoding.trackme.R;
import net.simplifiedcoding.trackme.activities.TrackFriendActivity;
import net.simplifiedcoding.trackme.constants.Constants;
import net.simplifiedcoding.trackme.models.User;

import java.util.List;

/**
 * Created by Belal on 3/4/2018.
 */

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder> {

    Context mCtx;
    List<User> userList;


    public UsersAdapter(Context mCtx, List<User> userList) {
        this.mCtx = mCtx;
        this.userList = userList;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.recyclerview_users, null);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        User user = userList.get(position);
        holder.textViewName.setText(user.name);

        Glide.with(mCtx)
                .load(user.avatarUrl)
                .into(holder.imageViewAvatar);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageViewAvatar;
        TextView textViewName;

        public UserViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imageViewAvatar = itemView.findViewById(R.id.imageViewAvatar);
            textViewName = itemView.findViewById(R.id.textViewUserName);
        }

        @Override
        public void onClick(View v) {
            User user = userList.get(getAdapterPosition());

            Intent intent = new Intent(mCtx, TrackFriendActivity.class);
            intent.putExtra(Constants.KEY_USER_ID, user.uid);

            mCtx.startActivity(intent);
        }
    }
}
